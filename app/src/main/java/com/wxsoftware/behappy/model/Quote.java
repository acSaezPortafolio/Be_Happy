package com.wxsoftware.behappy.model;

/**
 * Created by alberto on 20/02/18.
 */

public class Quote {

    private String quote;
    private String author;

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
