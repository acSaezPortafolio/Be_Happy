package com.wxsoftware.behappy.data;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.wxsoftware.behappy.controller.AppController;
import com.wxsoftware.behappy.model.Quote;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alberto on 20/02/18.
 */

public class QuoteData {

    private static final String TAG="Charging JSON Array: ";

    List<Quote> quoteArrayList=new ArrayList<>();

    public void getQuotes(final QuoteListAsyncResponse callBack){

        String url="https://raw.githubusercontent.com/pdichone/UIUX-Android-Course/master/Quotes.json%20";


            JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        //vamos a capturar cada objeto del array en el JSON
                        for (int i=0; i<response.length(); i++){
                            try {

                                JSONObject quoteObject=response.getJSONObject(i);
                                Quote quote=new Quote();
                                quote.setQuote(quoteObject.getString("quote"));
                                quote.setAuthor(quoteObject.getString("name"));

                                Log.d("Nota: ", quoteObject.getString("name"));

                                quoteArrayList.add(quote);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }//fin del for

                        if(callBack!=null) callBack.processFinished(quoteArrayList);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Cannot access to the url successfully.");
            }
        });

        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
    }
}
