package com.wxsoftware.behappy.data;

import com.wxsoftware.behappy.model.Quote;

import java.util.List;

/**
 * Created by alberto on 21/02/18.
 */

public interface QuoteListAsyncResponse {

    void processFinished(List<Quote> quotes);
}
