package com.wxsoftware.behappy.controller;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.wxsoftware.behappy.MainActivity;

/**
 * Created by alberto on 20/02/18.
 */

public class AppController extends Application {

    public static final String TAG=AppController.class.getSimpleName();

    private static AppController mInstance;
    private RequestQueue mRequestQueue;


    public static synchronized AppController getInstance(){
            return mInstance;
    }

    @Override
    public void onCreate(){
        super.onCreate();

        mInstance=this;
    }


    public RequestQueue getRequestQueue(){
        if(mRequestQueue==null)
            mRequestQueue= Volley.newRequestQueue(getApplicationContext());
        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag){
        req.setTag(TextUtils.isEmpty(tag)?TAG:tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req){
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequest(Object tag){
        if(mRequestQueue!=null){
            mRequestQueue.cancelAll(tag);
        }
    }
}
