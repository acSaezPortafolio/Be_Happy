package com.wxsoftware.behappy;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.wxsoftware.behappy.data.QuoteData;
import com.wxsoftware.behappy.data.QuoteListAsyncResponse;
import com.wxsoftware.behappy.data.QuoteViewPagerAdapter;
import com.wxsoftware.behappy.model.Quote;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private QuoteViewPagerAdapter quoteViewPagerAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        quoteViewPagerAdapter=new QuoteViewPagerAdapter(getSupportFragmentManager(), getFragments());

        viewPager=findViewById(R.id.viewPager);
        viewPager.setAdapter(quoteViewPagerAdapter);
    }


    private List<Fragment> getFragments(){
        final List<Fragment> fragmentList=new ArrayList<>();

        new QuoteData().getQuotes(new QuoteListAsyncResponse() {
            @Override
            public void processFinished(List<Quote> quotes) {

                for(int i=0; i<quotes.size(); i++){
                    QuoteFragment quoteFragment=QuoteFragment.newInstance(
                            quotes.get(i).getQuote(),
                            quotes.get(i).getAuthor()
                    );
                    fragmentList.add(quoteFragment);
                }

                quoteViewPagerAdapter.notifyDataSetChanged();
            }
        });

        return fragmentList;
    }




    public void shareClick(View view) {

        Intent shareIntent=new Intent();
        QuoteFragment fragment=(QuoteFragment) quoteViewPagerAdapter
                .getItem(viewPager.getCurrentItem());

        String textCard=fragment.getCardText();

        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, textCard );
        startActivity(Intent.createChooser(shareIntent, "Share Quote"));
    }
}
